<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = [
        'title', 'genre', 'description', 'release_date'
    ];

    public function genre()
    {
        return $this->belongsTo(Genre::class);
    }
}
