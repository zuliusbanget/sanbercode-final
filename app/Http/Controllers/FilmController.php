<?php

namespace App\Http\Controllers;

use App\Models\Film;
use Illuminate\Http\Request;
use App\Models\Genre;

class FilmController extends Controller
{
    public function index()
    {
        $films = Film::all();
        return view('films.index', compact('films'));
    }

    public function create()
    {
        $genres = Genre::all();
        return view('films.create', compact('genres'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'genre' => 'required',
            // tambahkan validasi lainnya sesuai kebutuhan
        ]);

        Film::create($request->all());

        return redirect('/films')->with('success', 'Film created successfully.');
    }

    public function show(Film $film)
    {
        return view('films.show', compact('film'));
    }

    public function edit(Film $film)
    {
        return view('films.edit', compact('film'));
    }

    public function update(Request $request, Film $film)
    {
        $request->validate([
            'title' => 'required',
            'genre' => 'required',
            // tambahkan validasi lainnya sesuai kebutuhan
        ]);

        $film->update($request->all());

        return redirect('/films')->with('success', 'Film updated successfully.');
    }

    public function destroy(Film $film)
    {
        $film->delete();

        return redirect('/films')->with('success', 'Film deleted successfully.');
    }
}
