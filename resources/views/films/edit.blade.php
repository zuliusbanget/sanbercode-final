@extends('layout.master')

@section('title')
    Film Edit
@endsection

@section('content')
    <h1>Edit Film</h1>
    
    <form action="{{ route('films.update', $film) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="{{ $film->title }}">
        </div>
        <div class="form-group">
            <label>Genre</label>
            <select name="genre" class="form-control">
                @foreach($genres as $genre)
                    <option value="{{ $genre->id }}" {{ $film->genre_id == $genre->id ? 'selected' : '' }}>{{ $genre->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" class="form-control">{{ $film->description }}</textarea>
        </div>
        <div class="form-group">
            <label>Release Date</label>
            <input type="date" name="release_date" class="form-control" value="{{ $film->release_date }}">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
