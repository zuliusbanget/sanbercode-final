<!-- resources/views/films/index.blade.php -->
@extends('layout.master')

@section('title')
    Film List
@endsection

@section('content')
    <h1>Film List</h1>
    <a href="{{ route('films.create') }}" class="btn btn-primary">Add Film</a>
    
    <table class="table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Genre</th>
                <th>Description</th>
                <th>Release Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($films as $film)
                <tr>
                    <td>{{ $film->title }}</td>
                    <td>{{ $film->genre->name }}</td>
                    <td>{{ $film->description }}</td>
                    <td>{{ $film->release_date }}</td>
                    <td>
                        <a href="{{ route('films.show', $film) }}" class="btn btn-info">View</a>
                        <a href="{{ route('films.edit', $film) }}" class="btn btn-primary">Edit</a>
                        <form action="{{ route('films.destroy', $film) }}" method="POST" style="display: inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
