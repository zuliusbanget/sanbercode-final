@extends('layout.master')

@section('title')
    Film Detil
@endsection

@section('content')
    <h1>Film Details</h1>
    
    <p><strong>Title:</strong> {{ $film->title }}</p>
    <p><strong>Genre:</strong> {{ $film->genre->name }}</p>
    <p><strong>Description:</strong> {{ $film->description }}</p>
    <p><strong>Release Date:</strong> {{ $film->release_date }}</p>
    
    <a href="{{ route('films.edit', $film) }}" class="btn btn-primary">Edit</a>
    
    <form action="{{ route('films.destroy', $film) }}" method="POST" style="display: inline-block">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
    </form>
@endsection
