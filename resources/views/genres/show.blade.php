@extends('layout.master')

@section('title')
    Film Show
@endsection

@section('content')
    <h1>Genre Details</h1>
    <p><strong>ID:</strong> {{ $genre->id }}</p>
    <p><strong>Name:</strong> {{ $genre->name }}</p>
    <a href="{{ route('genres.edit', $genre->id) }}" class="btn btn-primary">Edit</a>
    <form action="{{ route('genres.destroy', $genre->id) }}" method="POST" style="display: inline-block;">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this genre?')">Delete</button>
    </form>
@endsection
