@extends('layout.master')

@section('title')
    Film List
@endsection

@section('content')
    <h1>Genres</h1>
    <a href="{{ route('genres.create') }}" class="btn btn-primary">Create Genre</a>
    <br><br>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($genres as $genre)
            <tr>
                <td>{{ $genre->id }}</td>
                <td>{{ $genre->name }}</td>
                <td>
                    <a href="{{ route('genres.show', $genre->id) }}" class="btn btn-info">View</a>
                    <a href="{{ route('genres.edit', $genre->id) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('genres.destroy', $genre->id) }}" method="POST" style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this genre?')">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection
